// cypress/integration/loginWithNumberNoOtp.spec.js
import LoginPage from '../support/pageobject/LoginPage';

describe('Login', () => {
    const loginPage = new LoginPage();

    beforeEach(() => {
        loginPage.visit();
    });

    it('should login with valid input', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterPhoneNumber(data.validPhoneNumber);
            loginPage.submit();
            loginPage.getpage(); 
        });
    });

    it('should show error with an invalid phone number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterPhoneNumber(data.invalidPhoneNumber);
            loginPage.submit();
            loginPage.getNotificationMessage() // Adjust according to your application
        });
    });

    it('should show error to null phone number', () => {
        loginPage.submit();
        loginPage.getFieldMessages() 
    });

    it('should show error for unregistered number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterPhoneNumber(data.unregisteredPhoneNumber);
            loginPage.submit();
            loginPage.getNotificationMessage() 
        });
    });

    it('should show error on minimum length of number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterPhoneNumber(data.shortPhoneNumber);
            loginPage.submit();
            loginPage.getFieldMessages()
        });
    });

    it('should show error on maximum length of number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterPhoneNumber(data.longPhoneNumber);
            loginPage.submit();
            loginPage.getFieldMessages() 
        });
    });
});
