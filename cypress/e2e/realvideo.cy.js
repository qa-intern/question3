import LoginPage from '../support/pageobject/loginPage';
import VideoPage from '../support/pageobject/videoPage';

describe('OTP Authentication', () => {
  const loginPage = new LoginPage();
  const videoPage = new VideoPage();

  beforeEach(() => {
    loginPage.visitLoginPage();
  });

  it.only('should successfully redirect to videos', () => {
    cy.fixture('loginData').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.verifyVideoPage();
    });
  });

  it('should successfully play videos', () => {
    cy.fixture('loginData').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.verifyVideoPage();
      videoPage.clickOnVideo();
    });
  });

  it('should successfully make video interactive', () => {
    cy.fixture('loginData').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.clickOnVideo();
      videoPage.toggleVideo();
    });
  });

  it('should successfully redirect to ask section', () => {
    cy.fixture('loginData').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.clickOnVideo();
      videoPage.goToAskSection();
    });
  });
});