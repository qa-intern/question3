class VideoPage {
    clickOnCourse() {
      cy.get(':nth-child(6) > .q-item__section--side > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click();
    }
  
    verifyVideoPage() {
      cy.get('.q-tab--active > .q-tab__content > .q-tab__label').should('be.visible');
    }
  
    clickOnVideo() {
      cy.get(':nth-child(1) > .carousel > .carousel__viewport > .carousel__track > .carousel__slide--active > .p-10 > .image > .relative-position > .q-img > .q-img__container > .q-img__image').click();
    }
  
    toggleVideo() {
      cy.get('.q-toggle__thumb').click();
    }
  
    goToAskSection() {
      cy.get('.content-actions > :nth-child(3) > .q-img > .q-img__container > .q-img__image').click();
    }
  }
  
  export default VideoPage;
  