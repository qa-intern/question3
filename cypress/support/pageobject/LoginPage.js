// cypress/support/pageobject/LoginPage.js
class LoginPage {
    visit() {
        cy.visit('https://app.ambition.guru/login');
    }

    enterPhoneNumber(phoneNumber) {
        cy.get('.q-field__control').type(phoneNumber);
    }

    submit() {
        cy.get('.q-btn__content').click();
    }

    getNotificationMessage() {
        return cy.get('.q-notification__message');
    }

    getFieldMessages() {
        return cy.get('.q-field__messages > div');
    }

    getpage() {
        return cy.get('.q-page');
    }
}

export default LoginPage;
