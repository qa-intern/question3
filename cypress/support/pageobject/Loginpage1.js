// cypress/support/pageobject/LoginPage.js
class LoginPage {
    visit() {
        cy.visit('https://app.ambition.guru/login');
    }

    enterPhoneNumber(phoneNumber) {
        cy.get('.q-field__control').type(phoneNumber);
    }

    submitPhoneNumber() {
        cy.get('.q-btn__content').click();
    }
}

export default LoginPage;
