class SearchPage {
    performSearch(searchTerm) {
      cy.get('.flex > .q-field > .q-field__inner > .q-field__control').click().type(`${searchTerm}{enter}`);
    }
  
    selectSearchResult() {
      cy.get('.q-field__control-container > .d-flex > .q-img > .q-img__container > .q-img__image').click();
    }
  }
  
  export default SearchPage;
