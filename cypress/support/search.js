// cypress/support/pageobject/search.js
class SearchPage {
    visit() {
        cy.visit('https://app.ambition.guru/login');
    }

    enterPhoneNumber(phoneNumber) {
        cy.get('.q-field__control').type(phoneNumber);
    }

    submitPhoneNumber() {
        cy.get('.q-btn__content').click();
    }

    enterSearchQuery(query) {
        cy.get('.flex > .q-field > .q-field__inner > .q-field__control')
            .click()
            .clear()
            .type(`${query}{enter}`);
    }

    clickFirstResult() {
        cy.get('.q-field__control-container > .d-flex > .q-img > .q-img__container > .q-img__image').click();
    }
}

export default SearchPage;
